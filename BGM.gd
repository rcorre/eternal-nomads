extends Node

var tween: Tween
var bgm: AudioStreamPlayer

func set_volume(val: float):
	AudioServer.set_bus_volume_db(0, val)

func _ready():
	bgm = AudioStreamPlayer.new()
	bgm.stream = preload("res://assets/music/lost_in_the_ether.ogg")
	bgm.play()
	add_child(bgm)
	tween = Tween.new()
	add_child(tween)

func start():
	set_volume(-30)
	tween.interpolate_method(
		self, "set_volume", -30, 0,
		3.0, Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	tween.start()

func fade():
	tween.interpolate_method(
		self, "set_volume", 0, -30,
		3.0, Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	tween.start()
