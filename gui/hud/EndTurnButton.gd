extends Button

func _ready():
	connect("pressed", self, "_on_pressed")

func _on_pressed():
	Player.end_turn()
	disabled = true
	yield(Player, "turn_started")
	disabled = false
