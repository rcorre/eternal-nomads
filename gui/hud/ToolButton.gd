extends Button

enum Building {
	NONE,
	SOLAR,
	FACTORY,
	HAB,
}

export(Building) var building

func _ready():
	connect("toggled", self, "_on_toggled")

func _on_toggled(on: bool):
	Player.build_selection = building if on else Building.NONE

func _process(delta):
	var energy_cost := 0
	var mineral_cost := 0
	var person_cost := 0
	match building:
		Building.SOLAR:
			mineral_cost = 1
			person_cost = 1
		Building.FACTORY:
			energy_cost = 1
			person_cost = 1
		Building.HAB:
			energy_cost = 1
			mineral_cost = 1

	disabled = Player.energy < energy_cost or Player.minerals < mineral_cost or Player.population < person_cost
