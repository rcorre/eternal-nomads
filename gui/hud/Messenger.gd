extends Label

func _ready():
	Player.connect("turn_started", self, "_on_turn_started")

func _on_turn_started():
	match Player.turn_count:
		1:
			text = "Time passes ..."
		2:
			text = "Cities fall into night"
		3:
			text = "Mourn not the lost"
		4:
			text = "Lives emerge in the light"
		_:
			return
	
	$AnimationPlayer.play("Type")
