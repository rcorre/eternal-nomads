extends Label

onready var anim_player: AnimationPlayer = $AnimationPlayer

func _ready():
	Player.connect("population_changed", self, "_on_amount_changed")

func _on_amount_changed(amount: int):
	text = str(amount)
	anim_player.play("Change")
