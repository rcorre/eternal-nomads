# Lux Jam Entry

This is my entry for the [Lux Game Jam](https://itch.io/jam/lux-jam).
The theme is "Light and Shadow".

# Credits

## Fonts

This game uses the [Orbitron](https://www.theleagueofmoveabletype.com/orbitron) font,
licensed under the [Open Font License](./common/Open Font License.markdown).
