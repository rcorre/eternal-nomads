extends Spatial

func _ready():
	var basic_prob := max(0.7 - 0.05 * Player.turn_count, 0.01)
	for c in get_children():
		var scene: PackedScene
		var r := randf()
		if r <= basic_prob:
			scene = preload("res://terrain/BasicTile.tscn")
		elif r <= basic_prob + 1 * (1 - basic_prob) / 3.0:
			scene = preload("res://terrain/Tree.escn")
		elif r <= basic_prob + 2 * (1 - basic_prob) / 3.0:
			scene = preload("res://terrain/Water.escn")
		else:
			scene = preload("res://terrain/Mountain.escn")

		c.add_child(scene.instance())
