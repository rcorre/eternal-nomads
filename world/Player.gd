extends Node

enum Building {
	NONE,
	SOLAR,
	FACTORY,
	HAB,
}

signal energy_changed(amount)
signal minerals_changed(amount)
signal population_changed(amount)
signal turn_ended()
signal turn_started()

var energy := 0 setget set_energy
var minerals := 0 setget set_minerals
var population := 0 setget set_population
var build_selection = Building.NONE setget set_build_selection
var turn_count := 0

func set_build_selection(val: int):
	build_selection = val

func set_population(val: int):
	population = val
	emit_signal("population_changed", population)

func set_energy(val: int):
	energy = val
	emit_signal("energy_changed", energy)

func set_minerals(val: int):
	minerals = val
	emit_signal("minerals_changed", minerals)

func build() -> Node:
	var scene: PackedScene
	var energy_cost := 0
	var mineral_cost := 0
	var person_cost := 0
	if energy > 0:
		match build_selection:
			Building.SOLAR:
				mineral_cost = 1
				person_cost = 1
				scene = preload("res://buildings/SolarPanel/SolarPanel.tscn")
			Building.FACTORY:
				energy_cost = 1
				person_cost = 1
				scene = preload("res://buildings/Factory/Factory.tscn")
			Building.HAB:
				energy_cost = 1
				mineral_cost = 1
				scene = preload("res://buildings/Hab/Hab.tscn")

	if scene and energy >= energy_cost and minerals >= mineral_cost and population >= person_cost:
		set_energy(energy - energy_cost)
		set_minerals(minerals - mineral_cost)
		set_population(population - person_cost)
		return scene.instance()

	return null

func start_turn():
	turn_count += 1
	emit_signal("turn_started")
	if population == 0:
		get_tree().current_scene.add_child(preload("res://GameOver.tscn").instance())
		BGM.fade()

func end_turn():
	set_energy(0)
	set_population(0)
	emit_signal("turn_ended")
