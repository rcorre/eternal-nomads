extends Spatial

const TURN_SPEED := 0.5
const ROW_SCENE := preload("res://world/TileRow.tscn")

onready var rows: Node = $Rows

var tween := Tween.new()

func _ready():
	add_child(tween)
	Player.connect("turn_ended", self, "_on_turn_ended")
	var center_row := rows.get_child(rows.get_child_count() / 2)
	var center_tile := center_row.get_child(0)
	var basic_tile := preload("res://terrain/BasicTile.tscn").instance()
	center_tile.replace_by(basic_tile)
	basic_tile.building = preload("res://buildings/SpaceShip.tscn").instance()
	basic_tile.get_node("Cube").add_child(basic_tile.building)

func _on_turn_ended():
	rows.add_child(ROW_SCENE.instance())
	for row in rows.get_children():
		if not row as Spatial:
			continue
		tween.interpolate_property(
			row, "translation",
			row.translation, row.translation + Vector3(-2, 0, 0),
			TURN_SPEED, Tween.TRANS_LINEAR, Tween.EASE_IN
		)
	tween.start()
	yield(tween, "tween_all_completed")
	rows.get_child(0).queue_free()
	Player.start_turn()
