extends Spatial

var building: Node

func _on_MouseArea_clicked():
	if get_tree().is_input_handled():
		return
	if not building:
		building = Player.build()
		if building:
			$Cube.add_child(building)
			$AnimationPlayer.play("Build")
			get_tree().set_input_as_handled()
