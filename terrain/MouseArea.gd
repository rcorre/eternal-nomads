extends Area

signal clicked

func _ready():
	connect("input_event", self, "_on_input_event")

func _on_input_event(_camera: Node, ev: InputEvent, _pos: Vector3, _norm: Vector3, _shape_idx: int):
	var click_ev := ev as InputEventMouseButton
	if click_ev and click_ev.is_pressed() and click_ev.button_index == BUTTON_LEFT:
		emit_signal("clicked")

