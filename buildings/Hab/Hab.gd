extends Spatial

onready var anim_player: AnimationPlayer = $AnimationPlayer

func _ready():
	Player.connect("turn_started", self, "_on_turn_started")

func _on_turn_started():
	anim_player.play("StartTurn")
	Player.population += 1
