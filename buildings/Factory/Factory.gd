extends Spatial

onready var anim_player: AnimationPlayer = $AnimationPlayer

func _ready():
	Player.connect("turn_started", anim_player, "play", ["TurnStarted"])

func _on_Area_clicked():
	if get_tree().is_input_handled():
		return
	if Player.energy > 0 and Player.population > 0:
		Player.energy -= 1
		Player.population -= 1
		Player.minerals += 1
		get_tree().set_input_as_handled()
		anim_player.play("Produce")
		yield(anim_player, "animation_finished")

func _process(delta):
	var ok = Player.population > 0 and Player.energy > 0
	$Outline.visible = ok
	$Hint.visible = ok
