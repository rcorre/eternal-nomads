extends Spatial

onready var anim_player: AnimationPlayer = $AnimationPlayer

func _ready():
	Player.connect("turn_started", self, "_on_turn_started")

func _on_turn_started():
	Player.energy += 1
	anim_player.play("StartTurn")
